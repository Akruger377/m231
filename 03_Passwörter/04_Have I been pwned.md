# Have I been pwned?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung, Vertiefungsfragen |
| Zeitbudget  |  15 Minuten |
| Ziel | Die Frage "Lassen sich meine Benutzerdaten in geleakten Datendiebstähle finden?" für sich persönlich beantworten.  |

Keine Infrastruktur ist sicher. Es spielt keine Rolle wie viele Sicherheitsmechanismen vorhanden sind. Sobald ein System mit dem Internet verbunden wird, ist es nur eine Frage der Zeit bis ein Angriff versucht wird und auch erfolgreich ist. Am Besten lässt sich das Beispiel von namhaften Internationalen Firmen zeigen, die in den letzten Jahren grosse Datendiebstähle verzeichnet haben. 

Verschiedene Organisationen bieten einen kostenlosen Service an, der es Ihnen erlaubt zu prüfen, ob Ihre Daten in einem dieser Datendiebstähle vorhanden sind. 

Prüfen Sie mithilfe von [';--have i been pwned?](https://haveibeenpwned.com/) ob Sie bereits Opfer eines Datendiebstahls geworden sind. 

## Quellen
 - [wikipedia - List of data breaches](https://en.wikipedia.org/wiki/List_of_data_breaches)
 - [Adobe Breach Impacted At Least 38 Million Users](https://web.archive.org/web/20210817162148/https://krebsonsecurity.com/2013/10/adobe-breach-impacted-at-least-38-million-users/)

## Fragen
 - Weshalb macht es Sinn unterschiedliche Passwörter zu verwenden?
 - Mit welchen Ihrer persönlichen Zugangsdaten lässt sich am meisten Schaden anrichten?