# PGP - Pretty Good Privacy
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Klassenchallenge |
| Aufgabenstellung  | Nachrichten mit PGP verschlüsseln und Entschlüsseln. |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie lernen das CrypTool kennen. |

Pretty Good Privacy ist ein Programm zur Verschlüsselung und zum Unterschreiben von Daten. Es wird die Ver- und Entschlüsselung von E-Mails, Dateien, Ordner und ganzen Dateipartionen verwendet. 

## Aufgabenstellung (Teil 1)
 - Installieren Sie https://www.gpg4win.org/ auf Ihrem PC. <br/><i> - Das PGP Plugin für Outlook führte in der Vergangenheit immer wieder zu Problemen. Falls Ihr Outlook nicht mehr ordnungsgemäss funktioniert, aktualisieren sie GPG4WIN oder deaktivieren Sie das Plugin. <br/> - Sie müssen nichts spenden. Wählen Sie einfach 0 USD aus.</i>
 - Nach der Installation öffnet sich Kleopatra. Erstellen Sie mit *New Key Pair* ihr persönliches Schlüsselpaar. Geben Sie dabei die E-Mail Adresse ein mit der Sie das Schlüsselpaar verwenden möchten. 
 - Die öffentlichen Keyserver von OpenPGP funktionieren seit einer Weile nicht mehr. Deshalb müssen Sie die Schlüssel manuell austauschen. Schicken Sie Ihrem öffentlich Key (ASC Datei) an einer Ihren Mitlernenden. 
 - Nachdem beide Teilnehmer den öffentlichen Schlüssel des anderen importiert haben, können verschlüsselte Nachrichten bzw. Dateien ausgetauscht werden. Das entsprechende Vorgehen wird im Video (siehe unten) gezeigt. 

## Aufgabenstellung (Teil 2)
 - Verschlüsseln Sie mit dem öffentlichen Schlüssel der LP einen Text mit ihrem Namen und einem persönlichen Favoriten (z.B. Lieblingstier, Lieblingsautomarke, Lieblingskäse, usw.) und schicken Sie diesen per E-Mail an die Lehrperson. 

*Diese Aufgabe ist Teil der [Leistungsbeurteilung LB3](../99_Leistungsbeurteilung/).*

## Hinweise
 - Den Public Key müssen sie jeweils nur einmal mitschicken.
 - Verifizieren Sie den Fingerprint von erhaltenen Public-Keys (z.B. über einen zweiten Kanal: Telefon, Signal, Threema)
 - Kleopatra ist nicht die einzige Anwendung für PGP. Sie dürfen auch eine andere verwenden.

## Videoanleitung - Kleopatra - Windows - PGP Schlüsselpaar erstellen und Public-key exportieren
![PGP new keypair](videos/pgpnewkeypair.webm)

## ideoanleitung - Kleopatra - Windows - Public-Key einer dritten Person importieren
![PGP import public key](videos/pgpimportpublickey.webm)

## ideoanleitung - Kleopatra - Windows - Nachricht verschlüsseln und signieren
![PGP encrypt file](videos/pgpencrypt.webm)

## Videoanleitung - Kleopatra - Windows - Nachricht entschlüsseln und verifizieren
![PGP decrypt file](videos/pgpdecrypt.webm)

## Videoanleitung - Kleopatra - Windows - Fingerprint verifizieren
Um eine erfolgreiche Man-in-the-middle Attacke unwahrscheinlicher zu machen, empfiehlt es sich sehr den erhaltenen Fingerprint zu verifizieren. Dafür muss beim Importieren im Schritt *Certify* *Cancel* gewählt werden und anschliessend der Fingerprint manuell überprüft werden. Als Beispiel für eine "sichere" Übertragung: Der Public-Key schicken Sie per E-Mail und der Fingerprint schicken Sie per Threema (Chat-App). 

Fingerprint mithilfe von Powershell manuell überprüfen:<br/>
![PGP verify fingerprint](videos/pgpverifyfingerprint.webm)

Public Key im nachhinein manuell zertifizieren:<br/>
![PGP manually certify public key](videos/pgpcertifypublickey.webm)



