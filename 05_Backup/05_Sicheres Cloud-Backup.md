# Cloudbackup - aber sicher
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit, Austausch |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  20 Minuten |
| Ziele | Sie ein verschlüsseltes Backup auf einem Cloudspeicher eingerichtet. |

Die Cloud ist praktisch und die meisten Accounts kostenlos. Mit Ihrem Microsoft-Account, den Sie von der Schule bekommen haben, haben Sie über OneDrive mindestens 100 GB, theoretisch sogar 1 TB (=1000 GB) zur Verfügung. Von Apple bekommt man iCloud gratis 5 GB, was bald einmal nicht mehr reicht wenn man Fotos sichern will und mit 1 CHF pro Monat gibts dann schon 50 GB. Bei Google Drive erhält man man gratis 15 GB (Stand 10.9.2023), DropBox 2 GB. Das reicht für einige Dokumente und auch ein wenig Bilder. Bezahlt man bei DropBox zum Beispiel 120 Euro pro Jahr erhält man 2 TB Speicherplatz. 

Externe USB-Festplatten mit 256 GB bekommt man schon ab 30 CHF (Aug. 2023). Nach bereits einem halben Jahr, wäre das die günstigste Backup-Lösung. Doch ist es auch die bessere?

Vorteile Cloud:
 - Immer übers Internet erreichbar
 - Keine Hardware
 - Über alle gängigen Geräte-Typen erreichbar
 - Weitere Funktionen: Dateien-Teilen, usw. 

Nachteile Cloud:
 - Ab einer gewissen Menge Daten: Regelmässige Kosten
 - Datenschutzbedenken, Zugriff von "Unbekannt"? Erfahre ich davon?
 - Kein physischer Zugang zu den Daten: Abhängig von externer Firma

Deshalb eppfielt es sich eine "plurale" Backupstrategie zu fahren: **Lokale Sicherung + Cloud Sicherung**

Es gibt verschiedene Tools für die Erstellung eines Backups auf einem Cloudspeicher. 

Zwei bekannte OpenSource Tools sind:
 - [Duplicati](https://www.duplicati.com/)
 - [Duplicacy](https://duplicacy.com/)

Hier finden Sie einen Performancenvergleich: https://forum.duplicati.com/t/big-comparison-borg-vs-restic-vs-arq-5-vs-duplicacy-vs-duplicati/9952


## Aufgabenstellung

### Informieren & Planen

 1. Schauen Sie sich die oben aufgeführten Tools an. Kennen Sie noch andere Tools?
 2. Wählen Sie ein Tool aus, dass ihren Anforderungen entspricht. Mögliche Anforderungen: Es unterstützt mein Cloudspeicher (z.B. Google Drive)
 3. Suchen Sie auf der Webseite einer Bedienungsanleitung: Wie richten Sie das gewählte Backup-Tool auf Ihrem PC ein?
 4. Setzen Sie sich ein Ziel (z.B. "Den Ordner *TBZ* auf meinem Laptop in meinem privaten Google Drive Account Sichern") und notieren Sie es sich in Ihrem Portfolio. 
 5. Machen Sie sich einen Plan: Wie gehen Sie vor?


### Ausführen

Führen Sie die Installation aus und versuchen Sie Ihr Ziel zu erreichen. Wenn Sie auf Probleme stossen, versuchen Sie zuerst es selbst mithilfe des Internets (Suchmaschine) zu lösen. Arbeiten Sie mit einem Kollegen zusammen. Wenn Sie überhaupt nicht mehr weiterkommen, fragen Sie die Lehrperson. 
